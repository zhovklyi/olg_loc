<?php

require dirname(__DIR__).'/vendor/autoload.php';

define('VIEWS_BLOCKS_PATH', dirname(__DIR__).'/views/');
define('UPLOADED_IMAGES_PATH', dirname(__DIR__).'/public/assets/photos/');


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__).'/src/');
$dotenv->load();
session_start();
