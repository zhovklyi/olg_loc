<?php

namespace Controllers;

class AccountController extends Controller
{
    // Account page
    public function account()
    {
        $query = 'SELECT * FROM `users` WHERE `id` = :user_id;';
        $this->data['user'] = $this->db->select($query, ['user_id' => $_SESSION['user_id']])[0];

        $query = 'SELECT * FROM `animals` WHERE `user_id` = :user_id;';
        $this->data['account_animals'] = $this->db->select($query, ['user_id' => $_SESSION['user_id']]);

        $this->render->renderPage($this->data, ['account/account.php']);
    }

    // Log out
    public function logout()
    {
        unset($_SESSION['user_id']);
        unset($_SESSION['login']);
        unset($_SESSION['password']);
        header('Location: /');
    }

    // Registration page
    public function registration()
    {
        $data = [
            'login' => $_POST['login'] ?? false,
            'password' => $_POST['password'] ?? false,
            'email' => $_POST['email'] ?? false
        ];
        if ($this->isValid($data)) {
            $query = 'INSERT INTO `users` (`login`, `password`, `email`) VALUES (:login, :password, :email);';
            $args = [
                'login' => $_POST['login'],
                'password' => hash('sha1', $_POST['password']),
                'email' => $_POST['email'],
            ];
            $id = $this->db->insert($query, $args);
            if ($id) {
                $_SESSION['user_id'] = $id;
                $_SESSION['login'] = $_POST['login'];
                $_SESSION['pasword'] = $_POST['password'];
                header('Location: /');
            }
        }

        $this->render->renderPage($this->data, ['/account/registration.php']);
    }

    // Login page
    public function login()
    {
        if (isset($_POST['login']) && isset($_POST['password'])) {
            $query = 'SELECT * FROM `users` WHERE `login` = :login AND `password` = :password;';
            $args = [
                'login' => $_POST['login'],
                'password' => hash('sha1', $_POST['password']),
            ];
            $user = $this->db->select($query, $args);
            if ($user) {
                $_SESSION['user_id'] = $user[0]['id'];
                $_SESSION['login'] = $_POST['login'];
                $_SESSION['password'] = $_POST['password'];
                header('Location: /');
            }
        }
        $this->render->renderPage($this->data, ['/account/login.php']);
    }

    public function isValidData()
    {
        echo json_encode(['result' => $this->isValid($_POST)]);
    }

    public function isValid($data)
    {
        foreach ($data as $key => &$value) {
            if ($key === 'password'){
                continue;
            }
            if ($value == false){
                return false;
            }
            if (in_array($key, ['login', 'email'])){
                $query = "SELECT `$key` FROM `users` WHERE `$key` = :value;";
                $check = $this->db->select($query, ['value' => $value]);
                if ($check) {
                    return false;
                }
                continue;
            }
            return false;
        }
        return true;
    }
}
