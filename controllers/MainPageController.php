<?php

namespace Controllers;

class MainPageController extends Controller
{
    // Displays animals by type
    public function typeAnimals($animal_type)
    {
        $this->data['selected'] = $animal_type;

        $query = 'SELECT * FROM `animals` WHERE `animal_type_id` = (SELECT `id` FROM `animal_types` WHERE `code_name` = :animal_type) AND `status_id` = 1;';
        $this->data['animals'] = $this->db->select($query, ['animal_type' => $animal_type]);

        $this->render->renderPage($this->data, ['mainpage/animal_type_block.php']);
    }

    // Displays all animals
    public function animals()
    {
        foreach ($this->data['animal_types'] as &$animal_type) {
            $query = 'SELECT * FROM `animals` WHERE `animal_type_id` = :animal_type_id AND `status_id` = 1;';
            $animal_type['animals'] = $this->db->select($query, ['animal_type_id' => $animal_type['id']]);
        }
        $this->render->renderPage($this->data, ['mainpage/animals_block.php']);
    }

    // Displays greating on main page
    public function main()
    {
        $this->render->renderPage($this->data, ['mainpage/greating_block.php']);
    }
}
