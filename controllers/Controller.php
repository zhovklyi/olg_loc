<?php

namespace Controllers;

class Controller
{
    protected $db;
    protected $render;
    protected $data;
    protected $asserts;
    protected $predis;

    protected function prepareAnimalTypesData()
    {
        if ($this->predis->exists('animal_types')){
            $this->data['animal_types'] = json_decode($this->predis->get('animal_types'));
        } else {
            $query = 'SELECT * FROM `animal_types`;';
            $this->data['animal_types'] = $this->db->select($query);
            $this->predis->set('animal_types', json_encode($this->data['animal_types']));
        }
    }

    public function __construct(object $db, object $render, object $asserts, object $predis)
    {
        $this->db = $db;
        $this->render = $render;
        $this->asserts = $asserts;
        $this->predis = $predis;
        $this->prepareAnimalTypesData();
    }
}
