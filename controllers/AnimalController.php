<?php

namespace Controllers;
use Exceptions\FileExceptions;

class AnimalController extends Controller
{
    // Shows animal info
    public function show($animal_id)
    {
        $query = 'SELECT * FROM `animals` WHERE `id` = :animal_id;';
        $this->data['animal'] = $this->db->select($query, ['animal_id' => $animal_id])[0];

        $query = 'SELECT * FROM `users` WHERE `id` = :user_id;';
        $this->data['animal_owner'] = $this->db->select($query, ['user_id' => $this->data['animal']['user_id']])[0];

        $this->render->renderPage($this->data, ['animals/show_animal.php']);
    }

    // Edit page
    public function edit($animal_id)
    {
        if (!empty($_POST)) {
            $query = 'UPDATE `animals` SET `name` = :name, `price` = :price, `gender_id` = :gender_id, `animal_type_id` = :animal_type_id WHERE `id` = :id;';
            $args = [
                'name' => $_POST['name'],
                'price' => $_POST['price'],
                'gender_id' => $_POST['gender'],
                'animal_type_id' => $_POST['type'],
                'id' => $animal_id,
            ];

            if (!empty($_FILES)){
                try {
                    $file_info = pathinfo($_FILES['photo']['name']);

                    $this->asserts->assertFileExtension($file_info['extension']);
                    $this->asserts->assertFileSize($_FILES['photo']['size']);

                    $full_name = UPLOADED_IMAGES_PATH.'animal_photo_'.$animal_id.'.'.$file_info['extension'];
                    $name = '/assets/photos/animal_photo_'.$animal_id.'.'.$file_info['extension'];

                    move_uploaded_file($_FILES["photo"]["tmp_name"], $full_name);

                    $query = 'UPDATE `animals` SET `name` = :name, `price` = :price, `gender_id` = :gender_id, `animal_type_id` = :animal_type_id, `photo_url` = :photo_url WHERE `id` = :id;';
                    $args['photo_url'] = $name;
                } catch (FileExceptions $e) {
                    $this->data['errors'] = true;
                    $this->data['error_message'] = $e->getMessage();
                }
            }

            $this->db->insert($query, $args);
        }

        $query = 'SELECT * FROM `animals` WHERE `id` = :id;';
        $this->data['animal'] = $this->db->select($query, ['id' => $animal_id])[0];

        $query = 'SELECT * FROM `gender`;';
        $this->data['genders'] = $this->db->select($query);

        $this->render->renderPage($this->data, ['animals/edit_animal.php']);
    }

    public function activate($animal_id)
    {
        $query = 'UPDATE `animals` SET `status_id` = 1 WHERE `id` = :id';
        $this->db->insert($query, ['id' => $animal_id]);
        header('Location: /animal/'.$animal_id);
    }

    public function deactivate($animal_id)
    {
        $query = 'UPDATE `animals` SET `status_id` = 2 WHERE `id` = :id';
        $this->db->insert($query, ['id' => $animal_id]);
        header('Location: /animal/'.$animal_id);
    }
}
