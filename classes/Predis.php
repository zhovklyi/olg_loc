<?php
namespace Classes;

use Predis\Client;

class Predis
{
    protected $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function get($key)
    {
        return $this->client->hgetall($key);
    }

    public function set($key, $value)
    {
        $this->client->set($key, $value);
    }

    public function exists($key)
    {
        return $this->client->exists($key);
    }

    public function delete($key)
    {
        $this->client->del($key);
    }
}