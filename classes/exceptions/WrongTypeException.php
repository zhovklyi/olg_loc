<?php

namespace Exceptions;
use Exception;

class WrongTypeException extends FileExceptions
{
    public function __construct($message = "", $code = 0, $previous = null)
    {
        $this->message = 'Wrong file format!';
    }
}