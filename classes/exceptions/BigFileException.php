<?php
namespace Exceptions;
use Exception;

class BigFileException extends FileExceptions
{
    public function __construct($message = "", $code = 0, $previous = null)
    {
        $this->message = 'File size is too big!';
    }
}