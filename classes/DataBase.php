<?php

namespace Classes;

use PDO;

class DataBase
{
    private $pdo;

    public function __construct()
    {
        $dsn = 'mysql:host='.$_ENV['HOST'].';port='.$_ENV['PORT'].';dbname='.$_ENV['DB_NAME'].';charset=utf8mb4';
        $this->pdo = new PDO($dsn, $_ENV['DB_USER'], $_ENV['DB_PASSWORD']);
    }

    public function insert($query, $args)
    {
        $sql = $this->pdo->prepare($query);
        foreach ($args as $key => &$value) {
            $sql->bindParam(':'.$key, $value);
        }
        if (!$sql->execute()) {
            return false;
        }

        return $this->pdo->lastInsertId();
    }

    public function select($query, $args = [], $fetchMode = PDO::FETCH_ASSOC)
    {
        $sql = $this->pdo->prepare($query);
        foreach ($args as $key => &$value) {
            $sql->bindParam(':'.$key, $value);
        }

        if (!$sql->execute()) {
            return false;
        }

        return $sql->fetchAll($fetchMode);
    }
}
