<?php

namespace Classes;

use Exceptions\WrongTypeException;
use Exceptions\BigFileException;

class Asserts
{
    public function assertFileExtension($extension)
    {
        if (!in_array($extension, ['jpg', 'jpeg', 'png'])){
            throw new WrongTypeException;
        }
    }

    public function assertFileSize($file_size)
    {
        if ($file_size > 102135024){
            throw new BigFileException;
        }
    }
}