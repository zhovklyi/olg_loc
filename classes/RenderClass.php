<?php

namespace Classes;

class RenderClass
{
    public function renderPage($data, $blocks)
    {
        require_once VIEWS_BLOCKS_PATH.'top_part.php';

        foreach ($blocks as $block) {
            require_once VIEWS_BLOCKS_PATH.$block;
        }

        require_once VIEWS_BLOCKS_PATH.'buttom_part.php';
    }
}
