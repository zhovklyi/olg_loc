'use strict';

function validateEmale(email) {
    let regExp = /\S+@\S+\.\S+/;
    return regExp.test(email);
}

let inputs = document.querySelectorAll('#reg_form #login, #email');

for (let element of inputs) {
    element.onchange = function() {
        if (this.id == 'email' && (!validateEmale(this.value))) {
            if (this.classList.contains('available')) {
                this.classList.replace('available', 'notavailable');
            } else {
                this.classList.add('notavailable');
            }
            return;
        }

        let data = new FormData();
        data.append(this.id, this.value);
        fetch('/checkData', {
            method: 'POST',
            body: data
        }).then((response) => {
            return response.json();
        }).then((result) => {
            if (result['result']) {
                if (this.classList.contains('notavailable')) {
                    this.classList.replace('notavailable', 'available');
                    return;
                }
                this.classList.add('available');
                return;
            }
            if (this.classList.contains('available')) {
                this.classList.replace('available', 'notavailable');
                return;
            }
            this.classList.add('notavailable');
            return;
        });
    }
}