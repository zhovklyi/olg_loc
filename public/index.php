<?php
include dirname(__DIR__).'/phpscripts/config.php';

use Classes\DataBase;
use Classes\RenderClass;
use Classes\Asserts;
use Classes\Predis;

use Controllers\AccountController;
use Controllers\AnimalController;
use Controllers\MainPageController;

use Routers\RegExpRoute;
use Routers\Route;
use Routers\Router;

$database = new DataBase();
$render = new RenderClass();
$asserts = new Asserts();

$predis = new Predis();

$router = new Router($database, $render, $asserts, $predis);

// Main page controller routes
$router->addRoute(new Route(
    '/',
    ['controller' => MainPageController::class, 'method' => 'main']
));
$router->addRoute(new Route(
    '/animals',
    ['controller' => MainPageController::class, 'method' => 'animals']
));
$router->addRoute(new RegExpRoute(
    "/\/animals\/[a-z]{1,10}/",
    [
        'controller' => MainPageController::class,
        'method' => 'typeAnimals',
        'func' => function ($url) {
            return explode('/', $url)[2];
        },
    ]
));

// Account controller routes
$router->addRoute(new Route(
    '/login',
    [
        'controller' => AccountController::class,
        'method' => 'login',
    ]
));
$router->addRoute(new Route(
    '/registration',
    [
        'controller' => AccountController::class,
        'method' => 'registration',
    ]
));
$router->addRoute(new Route(
    '/logout',
    ['controller' => AccountController::class, 'method' => 'logout']
));
$router->addRoute(new Route(
    '/account',
    ['controller' => AccountController::class, 'method' => 'account']
));
$router->addRoute(new Route(
    '/checkData',
    ['controller' => AccountController::class, 'method' => 'isValidData']
));

// Animal controller routes
$router->addRoute(new RegExpRoute(
    "/\/edit\/animal\/[0-9]{1,10}/",
    [
        'controller' => AnimalController::class,
        'method' => 'edit',
        'func' => function ($url) {
            return explode('/', $url)[3];
        },
    ]
));
$router->addRoute(new RegExpRoute(
    "/\/activate\/animal\/[0-9]{1,10}/",
    [
        'controller' => AnimalController::class,
        'method' => 'activate',
        'func' => function ($url) {
            return explode('/', $url)[3];
        },
    ]
));
$router->addRoute(new RegExpRoute(
    "/\/deactivate\/animal\/[0-9]{1,10}/",
    [
        'controller' => AnimalController::class,
        'method' => 'deactivate',
        'func' => function ($url) {
            return explode('/', $url)[3];
        },
    ]
));
$router->addRoute(new RegExpRoute(
    "/\/animal\/[0-9]{1,10}/",
    [
        'controller' => AnimalController::class,
        'method' => 'show',
        'func' => function ($url) {
            return explode('/', $url)[2];
        },
    ]
));

$value = $router->proccess($_SERVER['REQUEST_URI']);
