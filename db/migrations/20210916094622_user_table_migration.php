<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class UserTableMigration extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('users');
        $table->addColumn('login', 'string');
        $table->addColumn('password', 'string');
        $table->addColumn('email', 'string');
        $table->addTimestamps();
        $table->addIndex(['login', 'email'], ['unique' => true]);
        $table->create();
    }
}
