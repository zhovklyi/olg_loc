<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class StatusTableMigration extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('status');
        $table->addColumn('status', 'string');
        $table->addTimestamps();
        $table->addIndex('status', ['unique' => true]);
        $table->create();
    }
}
