<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AnimalTypeMigration extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('animal_types');
        $table->addColumn('name', 'string');
        $table->addColumn('code_name', 'string');
        $table->addIndex(['name', 'code_name'], ['unique' => true]);
        $table->addTimestamps();
        $table->create();
    }
}
