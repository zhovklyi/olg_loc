<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class GenderTableMigration extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('gender');
        $table->addColumn('name', 'string');
        $table->addTimestamps();
        $table->addIndex('name', ['unique' => true]);
        $table->create();
    }
}
