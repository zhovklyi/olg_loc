<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AnimalsMigration extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('animals');

        $table->addColumn('name', 'string');
        $table->addColumn('gender_id', 'integer');
        $table->addColumn('animal_type_id', 'integer');
        $table->addColumn('photo_url', 'text', ['null' => true]);
        $table->addColumn('price', 'decimal', ['precision' => 10, 'scale' => 2]);
        $table->addColumn('user_id', 'integer');
        $table->addColumn('status_id', 'integer');
        $table->addTimestamps();

        $table->addForeignKey('gender_id', 'gender', 'id');
        $table->addForeignKey('animal_type_id', 'animal_types', 'id');
        $table->addForeignKey('user_id', 'users', 'id');
        $table->addForeignKey('status_id', 'status', 'id');

        $table->create();
    }
}
