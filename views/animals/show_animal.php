<div class="animal_content">
    <div class="back">
        <a onclick="history.back();"><img src="/assets/icons/left-arrow.png"></a>
    </div>
    <div class="animal_photos">
        <div class="animal_photo" style="background-image: url('<?php echo $data['animal']['photo_url']; ?>');"></div>
    </div>
    <div class="animal_info">
        <div class="info_row">
            <span>Name: <?php echo $data['animal']['name']; ?></span>
        </div>
        <div class="info_row">
            <span>Gender: <?php echo $data['animal']['gender_id']; ?></span>
        </div>
        <div class="info_row">
            <span>Type: <?php echo $data['animal']['animal_type_id']; ?></span>
        </div>
        <div class="info_row">
            <span>Price: <?php echo $data['animal']['price']; ?></span>
        </div>
    </div>
    <div class="animal_owner">
        <div class="info_row">
            <span>Owner email: <?php echo $data['animal_owner']['email']; ?></span>
        </div>
    </div>
    <div class="owner_controls">
        <?php
            if (isset($_SESSION['user_id']) && $_SESSION['user_id'] == $data['animal']['user_id']) {
                $editHref = '/edit/animal/'.$data['animal']['id'];

                $deactivateHref = (1 == $data['animal']['status_id']) ? '/deactivate/animal/' : '/activate/animal/';
                $deactivateHref .= $data['animal']['id'];
                $deactivateText = (1 == $data['animal']['status_id']) ? 'Deactivate' : 'Activate'; ?>
            <a href="<?php echo $editHref; ?>"><button>Edit</button></a>
            <a href="<?php echo $deactivateHref; ?>"><button><?php echo $deactivateText; ?></button></a>
        <?php
            } ?>
    </div>
</div>