<div class="edit_animal_content">
    <div class="back">
        <a onclick="history.back();"><img src="/assets/icons/left-arrow.png"></a>
    </div>
    <div class="animal_photos">
        <div class="animal_photo" style="background-image: url('<?php echo $data['animal']['photo_url']; ?>');"></div>
    </div>
    <div class="animal_info">
        <form action="/edit/animal/<?php echo $data['animal']['id']; ?>" method="POST" enctype="multipart/form-data">
            <div class="info_row">
                <label for="photo">Photo: </label>
                <input name="photo" id ="photo" type="file"/>
            </div>
            <div class="info_row">
                <label for="name">Name: </label>
                <input name="name" id="name" type="text" value="<?php echo $data['animal']['name']; ?>" required>
            </div>
            <div class="info_row">
                <label for="price">Price: </label>
                <input name="price" id="price" type="text" value="<?php echo $data['animal']['price']; ?>" required>
            </div>
            <div class="info_row">
                <label for="gender">Gender:</label>
                <select id="gender" name="gender">
                    <?php
                        foreach ($data['genders'] as $gender) {
                            $selected = ($gender['id'] == $data['animal']['gender_id']) ? ' selected' : ''; ?>
                        <option<?php echo $selected; ?> value="<?php echo $gender['id']; ?>"><?php echo $gender['name']; ?></option>
                    <?php
                        } ?>
                </select>
            </div>
            <div class="info_row">
                <label for="type">Type:</label>
                <select id="type" name="type">
                    <?php
                        foreach ($data['animal_types'] as $type) {
                            $selected = ($type['id'] == $data['animal']['animal_type_id']) ? ' selected' : ''; ?>
                        <option<?php echo $selected; ?> value="<?php echo $type['id']; ?>"><?php echo $type['name']; ?></option>
                    <?php
                        } ?>
                </select>
            </div>
            <div class="info_row submit_button">
                <button type="submit">Submit</button>
            </div>
        </form>
    </div>
</div>
<?php if (!empty($data['errors']) && $data['errors']){ ?>
    <script>alert('<?php echo $data['error_message']; ?>');</script>
<?php } ?>