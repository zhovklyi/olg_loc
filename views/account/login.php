<div class="login_box">
    <div class="image_side">
        <div class="transparent_block">
            <div class="content_block">
                <div class="registration_block">
                <h3>Don`t have account?<h3>
                <a href="/registration">Sign up</a>
                </div>
            </div>
        </div>
    </div>
    <div class="login_side">
        <div class="login_header">
            <h1>Sign in</h1>
        </div>
        <div class="login_form">
            <form action="/login" method="POST">
                <div class="input_row">
                    <label for="login">Login</label>
                    <input id="login" name="login" type="text" placeholder="Enter login" required>
                </div>

                <div class="input_row">
                    <label for="password">Password</label>
                    <input id="password" name="password" type="password" placeholder="Enter password" required>
                </div>

                <div class="input_row">
                    <input id="checkbox" type="checkbox">
                    <label for="checkbox" class="inline">Show password</label>
                </div>
                <div class="flex_controls">
                    <button type="submit">Sign in</button>
                </div>
            </form>
        </div>
    </div>
</div>