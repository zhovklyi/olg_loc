<div class="registration_box">
    <div class="image_side">
        <div class="transparent_block">
            <div class="content_block">
                <div class="login_block">
                <h3>Already have account?<h3>
                <a href="/login">Sign in</a>
                </div>
            </div>
        </div>
    </div>
    <div class="registration_side">
        <div class="registration_header">
            <h1>Sign up</h1>
        </div>
        <div class="registration_form">
            <form action="/registration" method="POST" id ="reg_form">
                <div class="input_row">
                    <label for="login">Login</label>
                    <input id="login" name="login" type="text" placeholder="Enter login" required>
                </div>

                <div class="input_row">
                    <label for="email">Email</label>
                    <input id="email" name="email" type="email" placeholder="Enter email" required>
                </div>

                <div class="input_row">
                    <label for="password">Password</label>
                    <input id="password" name="password" type="password" placeholder="Enter password" required>
                </div>

                <div class="input_row">
                    <input id="checkbox" type="checkbox">
                    <label for="checkbox" class="inline">Show password</label>
                </div>
                <div class="flex_controls">
                    <button type="submit">Sign up</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="/assets/js/validation_data.js"></script>