<div class="account_page">
    <div class="account_info">
        <div class="account_header">
            <h1>User info</h1>
        </div>
        <div class="info_row">
            <span>User login: <?php echo $data['user']['login']; ?></span>
        </div>
        <div class="info_row">
            <span>User password: <?php echo $data['user']['password']; ?></span>
        </div>
        <div class="info_row">
            <span>User email: <?php echo $data['user']['email']; ?></span>
        </div>
    </div>
    <div class="animal_container">
        <div class="animal_header">
            <h1>Account animals</h1>
        </div>
        <div class="account_animals">
            <?php foreach ($data['account_animals'] as $animal) { ?>
                <div class="animal">
                    <div class="animal_photo" style="background-image: url('<?php echo $animal['photo_url']; ?>');"></div>
                    <div class="animal_name"><?php echo $animal['name']; ?></div>
                    <div class="animal_price"><?php echo $animal['price']; ?></div>
                    <div class="animal_controls">
                        <?php
                            $pageHref = '/animal/'.$animal['id'];
                            $editHref = '/edit/animal/'.$animal['id'];

                            $activateHref = (1 == $animal['status_id']) ? '/deactivate' : '/activate';
                            $activateHref .= '/animal/'.$animal['id'];

                            $activateText = (1 == $animal['status_id']) ? 'Deactivate' : 'Activate';
                        ?>
                        <a href="<?php echo $pageHref; ?>"><button>View page</button></a>
                        <a href="<?php echo $editHref; ?>"><button>Edit animal</button></a>
                        <a href="<?php echo $activateHref; ?>"><button><?php echo $activateText; ?></button></a>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>