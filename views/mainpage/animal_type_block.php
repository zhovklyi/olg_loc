<div class="animal_type_container">
    <?php foreach ($data['animals'] as $animal) { ?>
        <div class="animal">
            <div class="animal_photo" style="background-image: url('<?php echo $animal['photo_url']; ?>');"></div>
            <div class="animal_name"><?php echo $animal['name']; ?></div>
            <div class="animal_price"><?php echo $animal['price']; ?></div>
            <div class="animal_controls"><a href="<?php echo '/animal/'.$animal['id']; ?>"><button>View page</button></a></div>
        </div>
    <?php } ?>
</div>