<div class="animals_block">
    <?php foreach ($data['animal_types'] as $animal_type) {
    if ($animal_type['animals']) { ?>
        <div class="animal_type_block">
            <div class="animal_type_header">
                <a href="<?php echo '/animals/'.$animal_type['code_name']; ?>"><button class="type_button"><?php echo $animal_type['name'].'s'; ?></button></a>
            </div>
            <div class="animals_row">
                <?php foreach ($animal_type['animals'] as $animal) { ?>
                    <div class="animal">
                        <div class="animal_photo" style="background-image: url('<?php echo $animal['photo_url']; ?>');"></div>
                        <div class="animal_name"><?php echo $animal['name']; ?></div>
                        <div class="animal_price"><?php echo $animal['price']; ?></div>
                        <div class="animal_controls"><a href="<?php echo '/animal/'.$animal['id']; ?>"><button>View page</button></a></div>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php }
} ?>
</div>