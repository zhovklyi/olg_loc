<div class="main_page_greating">
    <div class="greating_text">
        <h1>Welcome to Pets Shop</h1>
    </div>
    <div class="greating_info">
        <p>Hope you will find you future pet here!&#128522</p>
    </div>
    <div class="buttons">
        <a href="/animals"><button class="btn">Get started</button></a>
        <button class="btn-rounded"><img src="/assets/icons/down-arrow.png"></button>
    </div>
    <div class="greating_img">
        <img src="/assets/img/greating.png">
    </div>
</div>