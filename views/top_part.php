<!DOCTYPE html>
<head>
    <title>Pets Shop</title>
    <!-- Main css-->
    <link rel="stylesheet" href="/assets/main.css">
    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,400;0,700;0,900;1,100;1,400&family=Ubuntu:wght@700&display=swap" rel="stylesheet">
</head>
<body>
    <header>
        <a href="/">
            <div class="logo_area">
                <div class="logo">
                    <img src="/assets/img/logo.png">
                </div>
            </div>
        </a>
        <div class="navigation_area">
            <div class="navigation_row">
                <?php foreach ($data['animal_types'] as $animal_type) {?>
                    <?php
                        $href = ' href="/animals/'.$animal_type['code_name'].'"';
                        $class = (isset($data['selected']) && $data['selected'] == $animal_type['code_name']) ? ' class="active"' : '';
                    ?>
                    <a<?php echo $href; echo $class; ?>>
                        <div><?php echo $animal_type['name']; ?></div>
                    </a>
                <?php } ?>
            </div>
        </div>
        <div class="account_area">
            <?php
                $href = (isset($_SESSION['user_id'])) ? '/account' : '/login';
                $text = (!isset($_SESSION['user_id'])) ? 'Login' : 'Account';
            ?>
            <a href="<?php echo $href; ?>" class="account"><button><?php echo $text; ?></button></a>
            <?php if (isset($_SESSION['user_id'])) { ?>
                <a href="/logout" class="logout"><button>Log out</button></a>
            <?php } ?>
        </div>
    </header>
    <main>
        <div class="container">