<?php

require dirname(__DIR__).'/vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__).'/src/');
$dotenv->load();

$dsn = 'mysql:host='.$_ENV['HOST'].';port='.$_ENV['PORT'].';dbname='.$_ENV['DB_NAME'].';charset=utf8mb4';
$pdo = new PDO($dsn, $_ENV['DB_USER'], $_ENV['DB_PASSWORD']);

// Status table
$data = ['active', 'notactive'];

$query = 'INSERT INTO `status` (`status`) VALUES (:status);';
foreach ($data as $value) {
    $sql = $pdo->prepare($query);
    $sql->bindParam(':status', $value);
    $sql->execute();
}

// Users table
$data = [
    [
        'login' => 'zhovklyi',
        'password' => '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8',
        'email' => 'zhovklyi.vlad@gmail.com',
    ],
];
$query = 'INSERT INTO `users` (`login`, `password`, `email`) VALUES (:login, :password, :email);';
foreach ($data as $value) {
    $sql = $pdo->prepare($query);
    $sql->bindParam(':login', $value['login']);
    $sql->bindParam(':password', $value['password']);
    $sql->bindParam(':email', $value['email']);
    $sql->execute();
}

// Gender table
$data = ['Male', 'Feamle'];
$query = 'INSERT INTO `gender` (`name`) VALUES (:name);';
foreach ($data as $value) {
    $sql = $pdo->prepare($query);
    $sql->bindParam(':name', $value);
    $sql->execute();
}

// Animal types table
$data = [
    ['name' => 'Dog', 'code_name' => 'dogs'],
    ['name' => 'Cat', 'code_name' => 'cats'],
    ['name' => 'Parrot', 'code_name' => 'parrots'],
    ['name' => 'Hamsters', 'code_name' => 'hamsters'],
    ['name' => 'Fish', 'code_name' => 'fish'],
];
$query = 'INSERT INTO `animal_types`(`name`, `code_name`) VALUES (:name, :code_name);';
foreach ($data as $value) {
    $sql = $pdo->prepare($query);
    $sql->bindParam(':name', $value['name']);
    $sql->bindParam(':code_name', $value['code_name']);
    $sql->execute();
}

// Animals table
$data = [
    [
        'name' => 'Дарман',
        'gender_id' => 1,
        'animal_type_id' => 1,
        'photo_url' => 'https://cdn.social.org.ua/images/16253/16253-27969965-10213260-01111-0-1542360246-1542360252-1500-1-1542360252-728-b5f02a6ea6-1542441880.jpg',
        'price' => 1302.50,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Цезарь',
        'gender_id' => 1,
        'animal_type_id' => 1,
        'photo_url' => 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUWFRgWFRUZGBgaHBoaGhwaGBgaGhgYGhgaGRgYGhgcIS4lHB4rIRgYJjgmKy8xNTU1GiQ7QDs0Py40NTEBDAwMEA8QHhISHjQhISE0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NP/AABEIAOIA3wMBIgACEQEDEQH/xAAcAAACAwEBAQEAAAAAAAAAAAADBAIFBgABBwj/xAA1EAABAwMBBwIGAQQCAwEAAAABAAIRAwQhMQUSQVFhcYGR8AYiobHB0RMUMuHxUmIVQqIH/8QAGgEAAgMBAQAAAAAAAAAAAAAAAQIAAwQFBv/EACERAQEBAQADAQEBAQADAAAAAAABAhEDITESBEFREyIy/9oADAMBAAIRAxEAPwD5DC6FOF5CcA91eqULoUR6xOUKRPBKMblbP4Y2MakFzYHOJ9eSW3hpOktl7Ke9whhPRbjZXwwMS2Wn1HQrRbM2U1jRJDvurylRESMLPvyf8W5yzNP4ba0Y4adlC42Awwd0a5wtFcF7eEjoq7+uEkHCp15NT2smevdn7FYGlpaIKFU+HmBrhE6x+E5SusSmqdfeCr/8/fQ3x2MFf7Fgkgf6VBUoQ4hfUrm1Dgslf7KgkwrM76FyoKbU5TCm2zM6Ij6JHBMiTEVrUNjU6ynhChQgxRfSVza2UojrDBxxUlJcs2+ml3sWqZsRzyOA5/pCuvh88PXgnhLGUc1Q3VZ3Vi5vA+RqkixNKWoNYitYua1FaFOoi1imGKbQpBqlovlULoXoXLURGF61mVyYtKRc4AN3ugQqNB8P2LC5uC4890EDwV9R2PahgBAjxCzXwvs0AAuY9p5Ekx1WzY2BAKzeXS7GU3V5MR6Jl744wOKrqbnB06hFuLtpEOHp+ll/XV/5EF1mN7PXQjkEveMDw4gZI+omEvTaw4HiRHaMr17yMH3/AIS61wZn2VdULQG8tfCfsK/Poqus4TPVSZVgiFm1ffWiZ7OL/wDmCDUph0JCk8lO0nITdnwLiJNsmcl7W2W1w0RN+F6+6hX5/os+qteLvxTVNj5wm2WIAE8E0ytIlKX13Bjoprzd+JPH/h5haB5RRUbCpraoXmAe6sf6RxAyjjWqXWcw4y5GiN/NONFVOtH53TMeqBTut0wTkcFZNahfzKta2zmP1AJ68+qqbr4XbrvemfQcFY2l4rWnXBCuzuVXrPHzy52Q5pMZHZKm3I4L6XcWu8NPoqK/2C5xkfZW/m1VWQDFIK2rbCe3gkqlk8agpbLEfHAVIKAUgtZHoC0fw5s4l+87DR1H21VDRGdVpdkPaCAA9zuWAAl18GPp2yKzN0DI6aqyrPbGc9/2qTYlN5AxATt48DBWLy3jRidMhwGfzKBcgO1VLVqkmG4U6VR41WXVaJkyxm4Ykxw5dkarUBEHVA/nQwJVV0smQnySi0aaYZb9EfcE6cEn0/eIUsI7XxKA4ZAXrzj3qgn0Y15KXuq8BDM4Sty6VBkO0Lr5B2/Kptq3J3+/29lRZcEY659V1ch7ieA5eeKszC69U5s2uN7WBqesaKxZtgb+4DoB2WXfcbhgapSrdfxnffqfp/nir8qdT/W3feloEEEHrxxCR2pXlzX89VQWG2W1DAJ0kNyTPNzv8p7bLxuNzB5J9eiZh+lemQJV/YXKwlpUzqtTs1+iXN5Taz6bO0f1Cf3AVS2NTTCtqdQ8luxrsY9Zc+2aUhX2aw8FbBeESrSPx+CvQoBSBTAcsKLnuDW4X0L4ds6TIBl54nhKwuzDOBqePADuvoXw3bMaAXVHeAT4CXRo3Fk75dAByAS20GCJhPWe5uyGuI5u4pa/rYIgBYfPZGnx/VAWZkj0XOqqNZpnClQty45GVjtauIsaScJ2nTiExSt93XCK8CElifpJgwVAjI5qTDAQN/jyUCBvMHCG+TopDRx9PVc135Q4fr0CAOqBdU4Caa8eiXvXI8Sa9qatTjyoseWjdaP8FNVmSJOvAdEg4kFNKa+4g+1cPm+/6VXc7OfVdLqgHjT3CvGje1+vBLXFNrM7xPZW51xVqBWGwG0yHCuf+0AfMNSNdE1tWoAIBmOOJ8wqWvtXdPyjTjPHumjU3mb5Bk64we/JW67Z7JPVEtHmVq9mh0DMLJWGSIWw2aDAVY6aOykRKu7ZyorYlWlBxWrx1l3Fo1SS9N6IHLTKpsfj8Ju0s3PPIcScAdygfxkFH/nnBk/ZPKlzZ9a74ftqLSN3ee7oPlB7kZX0jZFE4/tbHYr5Nsqu6Q3fIOgDeC+g7AtQ3V7nOPMkpd3kTMbWphusqjvHydFatZDclVNwcrmee+2zxQkRlO2zW8oQC0ItN8aZVEi3VMV0u9+FxuOBQqp9PehTWBBd/wCVBL9QfCE6pghBFUEgjt9/2h+UHe4gHuvA6ACffuUJ7s+fyQhPqmI1yff0U/I9M06sa+fuoPqGSTjv+kp/KTn37wvH1ff4Q4YR+mThJV6YIxPon2ukTkJZ4nU47gT9FOGmlc2puniuqtY8Q6T5cPsiXLBwj1Sji7gnlSzqtv8AZ0ZYwd5/CHYXL2yx2h4FWLnPP/H6/tD/AKYnLgPSFbNevau5Htn7rgAtdsmoYErMW1rMayOWfVaTZmIDhlJf+pfjU2isqSq7Rys6RWnFZdGRURWPSsrt+FozVVj80VbdKPtzOAtG+1UP6UKrPkdT+vwSQtsOk9rt7/a+kfDFNzjvOG6PusPa0ocF9A+Hbd5ALgQ30lPrfY5We/rjS1v7VRXAzlXVxUEQqO8csHk5a2+MGq8RhVr77ddBQbu6jEn9Kpuqwjj3/CGcnvppHbQZEggnv+Akv/KCd3HgrL1KrQCSJ6Kj/wDLva4PDC1uYkGCBE546j1V+fFdT0qu5n6+g1rmMyhMuwSM/jt5VTR2gKtPeGCNRxBSDLtzTM5ykubPSyaljU1NoYmdNPJn7QoU74GfJ+iyVTaBjzPdOWFwXdkPxwer6ldSZM66D79B+kwyt0Ecp9yqK5rubhvqiWN+AYeT6j7RlT8B+mlZfDQjwh1L1sYZ6yq599SMzrzGD9cFAfecJkcj+UtyMPPc12dPol4z8qlRqg6fhFc0dvEJeHlL1qTuIQqQ5iffRM1iBxz5QaUzjKkGnLemJGvqZHYq7t2vEEw5p46HyNJ9FU0t7kPCtbCu5uIkcQeKaX/Kr1F7ZFW9JVlowat9FZ01ficZtDQoOxqpyoPWjKqvjDqaA+mnHlK1XLLmvRf1Y7DGzrfecPln7L6Dsud0D1WE2U472Ct5sofKIM9eCsva4VkzpO8JVNcuiZV5enys/e9Vn1Pa3N9KC/fJwq12zHvySYWiZbNcU+23ga4Rl4a1k7jYzW0KjyCS1pIWM/r3sLQP/RzXMPFpaZBadR4X164oDccNQQQQvnV7sHdfAbIJxqPscrX4dyS9Z/Ji6+B7E3nNfugAE6CYHDE8Ea9tS0YWk2VsZzGjEdEe42OXTJVG9y6tX4zzPGC/jMq02W8NJB1jHv1Vs7YxbwSN3ZuZ83LkEP1KP54W2jtLcGmeA/apBtJ+9vFs5P8A85PoM9F20ZMuPPTjqlf6p8z8uf8Aq39e4HILZjx557ZN6vfTSWFxTqDBh3JNfxkYJws7sai59YEHJ49dVp7lr6f97ZHMZ9RwVHlzJfS7x6tnaZtq+7iYKsP55/0qNldpEg++SKyqcAErNqL5Vi7JTNECeX2S1CiTkeiYptM+8pbDyrK2x1VvZsBAVLQaYx6K7sDgFCE0tbYEaK1pEFVdDVWNJaPGzaMQoPYph/NcVoiqviVWolHvle13qtrPKy5+vUf0TsX1hXAcAMnkvo2yKbtwElfKNiM+cGeK+qbKcd0SVok5Hn/LOaMXQVBearS1KUhVN5aqjWamdcVtpHEpwuAwlmUd05UnVBOqXg36dpMBBnTtKSt7Nj3zGJxOJ7T+EWtdAMgHJgaH7p25qMoW/wDJUHIADVzjoB74FNM2+on6mZ2g1gG40S9S4aBostf/ABQ5xPyFvEcfqoUNstf/AHYRvg1Enmyvw8OMQSlrm1DgRux3/KdsaRIDmRmMk/aF1zvZ3h2gz3VPLKtmpWH2ls2DgCFWUth7ztAB2/AK2Ny0b47+9VcW+ymPG9Eemquz5tZnCXx5vus5sqwZQEtBc8/+0R40TbqLn5d9VoX2LQMYShpNbx+p+yTWrb2pJJORm7zZGN5sgjPQpO3ZDhK1rmnXx/jCqr6xxvNEecKd6MWVgxpamK1ADTX79R1+6qLOsQI5fRPMuSe32SWDwZhgyFaWrkgwhO2+oI0Q/KWr6icAp1h4hV9scJuk9aMM+jjX8165qFvL1r1fFdfB670k45R6zkq45WbE9vSf0a9NHsBjRkwt9sy5BwF8qs6hkBfQfh6pgZkrXyccLye9NZwS1ZqKzTKFXOFRq9VyKq8YP8Kqe48I/StbnKQrNAVayJ2I3nNDjx94VT/+r3FRlO3DCd2XlwHMBgafQu9VZ0a38ZDjrxU/iu3Zc25bMHD2uGSx40MdQSCORKu8Mn66r8vx8mO1HBs8Y0Q2bSLhGhUbzZ1Vj91zZPTIKY2Vsxz3/O3cbzjJ7Ba+s76H8ENeaJc6YkwSMxPDsZT+0t4ZBnupbGaGUwxu7utgafXj+UW5ZxjPX8LD5s/+3Wvx3kZV1f54cIzzwtXY1AWj5uWuO3hZy9s5eHxDumJHY68URlURugjeHgkcuv8ApV8WdXt1VB0IPkn0P6SRE8DPn8pR1y3dyc57qFK6OhMjhOYUsCLJroBBPvqlKz46gqIrgiBw94XgnPWD08qSJ8BtwA/dOh0/Xvkmi3dPT3CC9g5Ryg/Y8lJ1TSffPCWw8vTLHkaKyt63p9lTNfHb8Ju1dPZBLGjtq6fa5Uls6E/Qq8FbiqdRa03r1yVY9emqVfKq4+GPQwxGIRqNKUmMu1/Rt1nTyFvfh8AAQsnaUQCtrsQADA8q3X/y5Vva0FPRBuCih+EvWCzUIrLh2cJQxMngm7iEi5knKXho8LN7MYSlVpB6cvom6lWMAJa5eYwM802bxLOqjamzXuG/SeJ/4n8HyVXto1ARvsIP/U4xhPvqPDpkqTbl/daM+X17VXA1pfuZqDpHcdRxU6u3ddcadf0oOfg/Kq2uJ4c8Ib1NDmcTubx9QyMD6rhTdhxMnn2SjazhomLd5d0VXw47nAifXuutpR6FGQTzHv7KbKUFJTROgM/XwrShTgGef0KRpU1Z0TgDmAD0IhDqWB1Lfdxrp6/5+6Vq0h6p24qDB46Hrw9+FVuqGSDp+dULTZjxrv8AP7Tlu6EoWyU5QYUplg2onaFaVX04IgqTCWlNNcJYvGVIXr38iqw1DEgr1t1Ouqf9E/L5ixidtqaHRYnrdmVoxGzz66ftqAwtNs4ACFSWjFeWQT6npjWrHIFw9TLkF3NZKBWo1JVWp2tzSoGZQpog2iEKuwFNQvAydVIlUtzaxlDZRVzcNBEJIUjJPhN0AgwQUhVpYlWj2YhLPZmOiPQ4pH0co1BkKwdbSosoQULTRKgYge/eUdrZKAxsHyisd78pKbIlPA+vvwUV1Tr086hL1j8o7n00/C83iR6JaeJVHz5+/sIVNvzD6om6psbojIloraOUxQEHoV60KYb76qWF6JuZUyJHVcx2i9qkASl4AdJ8aoFSpBj0XjnQYQahkZUNxmqDU7bqpZcpmhcroZg+XcaS2KtLZ6zdvdhWVveDmms7FUsaFr148pe3rb2im9yx7nKAVw5LtKlWP1UHFKKUqQOEKllFe7ghEoZEqDqfFSYM+9ES4CKFWs4oQpZTJbEKOAjAAFMT4Q2MEo7DkoTsHspRgZpSSlnNx77p4aFRLOKWjC7xLVwZx7IgbjxH5U4+UIHRLV3BTcMeJXOHvwjA6Oz3+1MPj6fRApv0I8qb9CR5CJRd7MKTzI9+qVFTn/tSbWEjqkpuB1CV4BKm9uqEgZ84ZcpyjUJ0QLO0mMStFb7HeQIauxMOdryWkGV3BM0b4jVWrNivA/tSVxswg5RuAnkrQ7Du98YV24LJ7EduOhasaLD5c800Y1+oXeJMpd5TVQYStRU1ZHtEwOp+y57pMBCY/K5jkDG6AxK9q5heNdDYXhfqDwRKFUdkJd51lGJwEvVMygLmO0Qqxz3KnTOvhBdkz1+iiQZ5geF6HfL3KhVOgQw/B6Z9TCFNBXCJXT8p951UHuXMPyn3ogIrDhc0YHlCov1HhSc75ffqjArxphED9UPryQ9/JHD2EQdUqddDK93gRKBOYRKQ4JbDdG/knK8DlAiFFj1OJ158ObIa4CQvoNjslgAwspsj5YWus70AZK7jmQStYMA0WU21ZNEkLUXl6yNVj9tbRbnKkCqKi8B61dtU3mrCf1G8/GkrcbLjcCx/0Rf4r6e1klX0VjVCr64WSxfKWa7XsvKD9VF51UaBxJQMdFRDad6QhOqTlStj9UDDTr6JaoeCO45jylKx1QCPabvk1UD+ETclsLwHMx0UowN7hPYIYdzXVBnuh1SgeJucisdw98UvOEVqkSupox08ITG5UyiQSmgOblMU1GqyHAoh0LcyOqnuxj0UzyK8eEOJ1EnCC5ucIu6vNxTgyrWkpVHmNT6rly7TmKq5qu5n1Kor955n1XLkKhSh/cFv9j/2DsuXLL5/i7xfTVXRV9dcuWRohR3HsgO08LlyU0RfoE3R0HZcuQO9dql/2uXIJEx/b75obvfquXIVIE7VAr6rlyIvDoe35TA0K5chBqdP+4KTtSuXJoSiM0Cnc8PC5cmKjUXOXLlB/wBRpqR1Xq5IZ//Z',
        'price' => 1302.50,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Одиссей',
        'gender_id' => 1,
        'animal_type_id' => 1,
        'photo_url' => 'https://www.purinaone.ru/dog/sites/purinaonedog.ru/files/2021-04/%D0%A0%D0%BE%D1%82%D0%B2%D0%B5%D0%B8%CC%86%D0%BB%D0%B5%D1%80%20%28506%D1%85379%29_2.jpg',
        'price' => 1302.50,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Эмир',
        'gender_id' => 1,
        'animal_type_id' => 1,
        'photo_url' => 'https://natworld.info/wp-content/uploads/2018/03/haski.jpeg',
        'price' => 1302.50,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Анда',
        'gender_id' => 2,
        'animal_type_id' => 1,
        'photo_url' => 'https://www.purina.ru/sites/purina.ru/files/2020-09/prichini_0.jpg',
        'price' => 1302.50,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Терра',
        'gender_id' => 2,
        'animal_type_id' => 1,
        'photo_url' => 'https://www.purinaone.ru/dog/sites/purinaonedog.ru/files/2020-07/zuby-u-sobak-mobile-min_1.jpg',
        'price' => 1302.50,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Эльба',
        'gender_id' => 2,
        'animal_type_id' => 1,
        'photo_url' => 'https://www.proplan.ru/sites/owners.proplan.ru/files/styles/nppe_article_listing_image_and_description/public/2020-03/bigl.jpg?itok=gFAdZf28',
        'price' => 1302.50,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Герда',
        'gender_id' => 2,
        'animal_type_id' => 1,
        'photo_url' => 'https://ru.mypetandi.com/sites/g/files/adhwdz671/files/styles/paragraph_image/public/2020-07/sobaka_v_polyah.jpg?itok=ETeeJJPI',
        'price' => 1302.50,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Эйс',
        'gender_id' => 1,
        'animal_type_id' => 1,
        'photo_url' => 'https://klike.net/uploads/posts/2019-06/medium/1559799916_2.jpg',
        'price' => 1302.50,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Аксель',
        'gender_id' => 1,
        'animal_type_id' => 1,
        'photo_url' => 'https://rubryka.com/wp-content/uploads/2021/01/tild3064-3938-4635-a364-306638353835__960.jpg',
        'price' => 1302.50,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Баки',
        'gender_id' => 1,
        'animal_type_id' => 1,
        'photo_url' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRadIqytXXQVLzeXoaVAIB9Wh4YV59-5VUqdw&usqp=CAU',
        'price' => 1302.50,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Бруно',
        'gender_id' => 1,
        'animal_type_id' => 1,
        'photo_url' => 'https://cdn.profile.ru/wp-content/uploads/2020/09/img_5f54dadd97ff6.jpeg',
        'price' => 1302.50,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Харли',
        'gender_id' => 2,
        'animal_type_id' => 1,
        'photo_url' => 'http://exvet.com.ua/share/file/97nksrnga7.jpg',
        'price' => 1302.50,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Лола',
        'gender_id' => 2,
        'animal_type_id' => 1,
        'photo_url' => 'https://vethelp72.ru/wp-content/uploads/2020/10/%D0%99%D0%BE%D1%80%D0%BA%D1%88%D0%B5%D1%80%D1%81%D0%BA%D0%B8%D0%B9-%D1%82%D0%B5%D1%80%D1%8C%D0%B5%D1%80-%D0%BF%D0%BE%D0%BD%D0%BE%D1%81.jpg',
        'price' => 1302.50,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Рокси',
        'gender_id' => 2,
        'animal_type_id' => 1,
        'photo_url' => 'https://images11.popmeh.ru/upload/img_cache/965/96554118c6eaea061361b4e754e4259c_ce_1591x848x0x2_cropped_666x444.jpg',
        'price' => 1302.50,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Виксен',
        'gender_id' => 2,
        'animal_type_id' => 1,
        'photo_url' => 'https://lh3.googleusercontent.com/proxy/XIuNyW0XNsdETxmTLii50feG_wYLNnXc-gGwGXkuKIyzFqW9_ff5kWadj02rycgWlgcVaN8MrW8pXv_77ogzJotFR-U3TLGH1PZoyNceHS_uRGFJ3jgdq095dhh_ujYC_ZChTS7kgX48ixvfveq__Z36R5CcBa7CVrTRM3uEq_5kl6yAXvPyTsuHWRbGkaR-W8269YweYpwuJI9AYb9kcn7G8JxY4ljj8ntM7zchBDju0oSFQTcIZqt402ty3vSVCO28X9F6SW5CnScFsLjrVQKb8g',
        'price' => 1302.50,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Соня',
        'gender_id' => 2,
        'animal_type_id' => 2,
        'photo_url' => null,
        'price' => 345.25,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Клеопатра',
        'gender_id' => 2,
        'animal_type_id' => 2,
        'photo_url' => null,
        'price' => 345.25,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Цунами',
        'gender_id' => 2,
        'animal_type_id' => 2,
        'photo_url' => null,
        'price' => 345.25,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Забияка',
        'gender_id' => 2,
        'animal_type_id' => 2,
        'photo_url' => null,
        'price' => 345.25,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Матильда',
        'gender_id' => 2,
        'animal_type_id' => 2,
        'photo_url' => null,
        'price' => 345.25,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Кнопка',
        'gender_id' => 2,
        'animal_type_id' => 2,
        'photo_url' => null,
        'price' => 345.25,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Масяня',
        'gender_id' => 2,
        'animal_type_id' => 2,
        'photo_url' => null,
        'price' => 345.25,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Царапка',
        'gender_id' => 2,
        'animal_type_id' => 2,
        'photo_url' => null,
        'price' => 345.25,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Серсея',
        'gender_id' => 2,
        'animal_type_id' => 2,
        'photo_url' => null,
        'price' => 345.25,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Ворсинка',
        'gender_id' => 2,
        'animal_type_id' => 2,
        'photo_url' => null,
        'price' => 345.25,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Гарфилд',
        'gender_id' => 1,
        'animal_type_id' => 2,
        'photo_url' => null,
        'price' => 345.25,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Том',
        'gender_id' => 1,
        'animal_type_id' => 2,
        'photo_url' => null,
        'price' => 345.25,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Гудвин',
        'gender_id' => 1,
        'animal_type_id' => 2,
        'photo_url' => null,
        'price' => 345.25,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Рокки',
        'gender_id' => 1,
        'animal_type_id' => 2,
        'photo_url' => null,
        'price' => 345.25,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Ленивец',
        'gender_id' => 1,
        'animal_type_id' => 2,
        'photo_url' => null,
        'price' => 345.25,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Пушок',
        'gender_id' => 1,
        'animal_type_id' => 2,
        'photo_url' => null,
        'price' => 345.25,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Спорти',
        'gender_id' => 1,
        'animal_type_id' => 2,
        'photo_url' => null,
        'price' => 345.25,
        'user_id' => 1,
        'status_id' => 1,
    ],
    [
        'name' => 'Бегемот',
        'gender_id' => 1,
        'animal_type_id' => 2,
        'photo_url' => null,
        'price' => 345.25,
        'user_id' => 1,
        'status_id' => 2,
    ],
];
$query = 'INSERT INTO `animals`(`name`, `gender_id`, `animal_type_id`, `photo_url`, `price`, `user_id`, `status_id`) VALUES (:name, :gender_id, :animal_type_id, :photo_url, :price, :user_id, :status_id);';
foreach ($data as &$value) {
    $sql = $pdo->prepare($query);
    $sql->bindParam(':name', $value['name']);
    $sql->bindParam(':gender_id', $value['gender_id']);
    $sql->bindParam(':animal_type_id', $value['animal_type_id']);
    $sql->bindParam(':photo_url', $value['photo_url']);
    $sql->bindParam(':price', $value['price']);
    $sql->bindParam(':user_id', $value['user_id']);
    $sql->bindParam(':status_id', $value['status_id']);
    $sql->execute();
}
