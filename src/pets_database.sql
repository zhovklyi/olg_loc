-- MySQL dump 10.13  Distrib 5.7.35, for Linux (x86_64)
--
-- Host: localhost    Database: pets_shop
-- ------------------------------------------------------
-- Server version	5.7.35-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `animal_types`
--

DROP TABLE IF EXISTS `animal_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `animal_types` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `code_name` (`code_name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `animal_types`
--

LOCK TABLES `animal_types` WRITE;
/*!40000 ALTER TABLE `animal_types` DISABLE KEYS */;
INSERT INTO `animal_types` VALUES (1,'Dog','dogs','2021-09-16 06:59:04','2021-09-16 06:59:04'),(2,'Cat','cats','2021-09-16 06:59:04','2021-09-16 06:59:04'),(3,'Parrot','parrots','2021-09-16 06:59:04','2021-09-16 06:59:04'),(4,'Hamsters','hamsters','2021-09-16 06:59:04','2021-09-16 06:59:04'),(5,'Fish','fish','2021-09-16 06:59:04','2021-09-16 06:59:04');
/*!40000 ALTER TABLE `animal_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `animals`
--

DROP TABLE IF EXISTS `animals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `animals` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `gender_id` int(25) NOT NULL,
  `animal_type_id` int(25) NOT NULL,
  `photo_url` longtext,
  `price` decimal(10,2) NOT NULL,
  `user_id` int(25) NOT NULL,
  `status_id` int(25) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `animal_type_id` (`animal_type_id`),
  KEY `gender_id` (`gender_id`),
  KEY `user_id` (`user_id`),
  KEY `status_id` (`status_id`),
  CONSTRAINT `animals_ibfk_1` FOREIGN KEY (`animal_type_id`) REFERENCES `animal_types` (`id`),
  CONSTRAINT `animals_ibfk_2` FOREIGN KEY (`gender_id`) REFERENCES `gender` (`id`),
  CONSTRAINT `animals_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `animals_ibfk_4` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `animals`
--

LOCK TABLES `animals` WRITE;
/*!40000 ALTER TABLE `animals` DISABLE KEYS */;
INSERT INTO `animals` VALUES (1,'Дарман',1,1,'https://cdn.social.org.ua/images/16253/16253-27969965-10213260-01111-0-1542360246-1542360252-1500-1-1542360252-728-b5f02a6ea6-1542441880.jpg',1302.50,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(2,'Цезарь',1,1,'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUWFRgWFRUZGBgaHBoaGhwaGBgaGhgYGhgaGRgYGhgcIS4lHB4rIRgYJjgmKy8xNTU1GiQ7QDs0Py40NTEBDAwMEA8QHhISHjQhISE0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NP/AABEIAOIA3wMBIgACEQEDEQH/xAAcAAACAwEBAQEAAAAAAAAAAAADBAIFBgABBwj/xAA1EAABAwMBBwIGAQQCAwEAAAABAAIRAwQhMQUSQVFhcYGR8AYiobHB0RMUMuHxUmIVQqIH/8QAGgEAAgMBAQAAAAAAAAAAAAAAAQIAAwQFBv/EACERAQEBAQADAQEBAQADAAAAAAABAhEDITESBEFREyIy/9oADAMBAAIRAxEAPwD5DC6FOF5CcA91eqULoUR6xOUKRPBKMblbP4Y2MakFzYHOJ9eSW3hpOktl7Ke9whhPRbjZXwwMS2Wn1HQrRbM2U1jRJDvurylRESMLPvyf8W5yzNP4ba0Y4adlC42Awwd0a5wtFcF7eEjoq7+uEkHCp15NT2smevdn7FYGlpaIKFU+HmBrhE6x+E5SusSmqdfeCr/8/fQ3x2MFf7Fgkgf6VBUoQ4hfUrm1Dgslf7KgkwrM76FyoKbU5TCm2zM6Ij6JHBMiTEVrUNjU6ynhChQgxRfSVza2UojrDBxxUlJcs2+ml3sWqZsRzyOA5/pCuvh88PXgnhLGUc1Q3VZ3Vi5vA+RqkixNKWoNYitYua1FaFOoi1imGKbQpBqlovlULoXoXLURGF61mVyYtKRc4AN3ugQqNB8P2LC5uC4890EDwV9R2PahgBAjxCzXwvs0AAuY9p5Ekx1WzY2BAKzeXS7GU3V5MR6Jl744wOKrqbnB06hFuLtpEOHp+ll/XV/5EF1mN7PXQjkEveMDw4gZI+omEvTaw4HiRHaMr17yMH3/AIS61wZn2VdULQG8tfCfsK/Poqus4TPVSZVgiFm1ffWiZ7OL/wDmCDUph0JCk8lO0nITdnwLiJNsmcl7W2W1w0RN+F6+6hX5/os+qteLvxTVNj5wm2WIAE8E0ytIlKX13Bjoprzd+JPH/h5haB5RRUbCpraoXmAe6sf6RxAyjjWqXWcw4y5GiN/NONFVOtH53TMeqBTut0wTkcFZNahfzKta2zmP1AJ68+qqbr4XbrvemfQcFY2l4rWnXBCuzuVXrPHzy52Q5pMZHZKm3I4L6XcWu8NPoqK/2C5xkfZW/m1VWQDFIK2rbCe3gkqlk8agpbLEfHAVIKAUgtZHoC0fw5s4l+87DR1H21VDRGdVpdkPaCAA9zuWAAl18GPp2yKzN0DI6aqyrPbGc9/2qTYlN5AxATt48DBWLy3jRidMhwGfzKBcgO1VLVqkmG4U6VR41WXVaJkyxm4Ykxw5dkarUBEHVA/nQwJVV0smQnySi0aaYZb9EfcE6cEn0/eIUsI7XxKA4ZAXrzj3qgn0Y15KXuq8BDM4Sty6VBkO0Lr5B2/Kptq3J3+/29lRZcEY659V1ch7ieA5eeKszC69U5s2uN7WBqesaKxZtgb+4DoB2WXfcbhgapSrdfxnffqfp/nir8qdT/W3feloEEEHrxxCR2pXlzX89VQWG2W1DAJ0kNyTPNzv8p7bLxuNzB5J9eiZh+lemQJV/YXKwlpUzqtTs1+iXN5Taz6bO0f1Cf3AVS2NTTCtqdQ8luxrsY9Zc+2aUhX2aw8FbBeESrSPx+CvQoBSBTAcsKLnuDW4X0L4ds6TIBl54nhKwuzDOBqePADuvoXw3bMaAXVHeAT4CXRo3Fk75dAByAS20GCJhPWe5uyGuI5u4pa/rYIgBYfPZGnx/VAWZkj0XOqqNZpnClQty45GVjtauIsaScJ2nTiExSt93XCK8CElifpJgwVAjI5qTDAQN/jyUCBvMHCG+TopDRx9PVc135Q4fr0CAOqBdU4Caa8eiXvXI8Sa9qatTjyoseWjdaP8FNVmSJOvAdEg4kFNKa+4g+1cPm+/6VXc7OfVdLqgHjT3CvGje1+vBLXFNrM7xPZW51xVqBWGwG0yHCuf+0AfMNSNdE1tWoAIBmOOJ8wqWvtXdPyjTjPHumjU3mb5Bk64we/JW67Z7JPVEtHmVq9mh0DMLJWGSIWw2aDAVY6aOykRKu7ZyorYlWlBxWrx1l3Fo1SS9N6IHLTKpsfj8Ju0s3PPIcScAdygfxkFH/nnBk/ZPKlzZ9a74ftqLSN3ee7oPlB7kZX0jZFE4/tbHYr5Nsqu6Q3fIOgDeC+g7AtQ3V7nOPMkpd3kTMbWphusqjvHydFatZDclVNwcrmee+2zxQkRlO2zW8oQC0ItN8aZVEi3VMV0u9+FxuOBQqp9PehTWBBd/wCVBL9QfCE6pghBFUEgjt9/2h+UHe4gHuvA6ACffuUJ7s+fyQhPqmI1yff0U/I9M06sa+fuoPqGSTjv+kp/KTn37wvH1ff4Q4YR+mThJV6YIxPon2ukTkJZ4nU47gT9FOGmlc2puniuqtY8Q6T5cPsiXLBwj1Sji7gnlSzqtv8AZ0ZYwd5/CHYXL2yx2h4FWLnPP/H6/tD/AKYnLgPSFbNevau5Htn7rgAtdsmoYErMW1rMayOWfVaTZmIDhlJf+pfjU2isqSq7Rys6RWnFZdGRURWPSsrt+FozVVj80VbdKPtzOAtG+1UP6UKrPkdT+vwSQtsOk9rt7/a+kfDFNzjvOG6PusPa0ocF9A+Hbd5ALgQ30lPrfY5We/rjS1v7VRXAzlXVxUEQqO8csHk5a2+MGq8RhVr77ddBQbu6jEn9Kpuqwjj3/CGcnvppHbQZEggnv+Akv/KCd3HgrL1KrQCSJ6Kj/wDLva4PDC1uYkGCBE546j1V+fFdT0qu5n6+g1rmMyhMuwSM/jt5VTR2gKtPeGCNRxBSDLtzTM5ykubPSyaljU1NoYmdNPJn7QoU74GfJ+iyVTaBjzPdOWFwXdkPxwer6ldSZM66D79B+kwyt0Ecp9yqK5rubhvqiWN+AYeT6j7RlT8B+mlZfDQjwh1L1sYZ6yq599SMzrzGD9cFAfecJkcj+UtyMPPc12dPol4z8qlRqg6fhFc0dvEJeHlL1qTuIQqQ5iffRM1iBxz5QaUzjKkGnLemJGvqZHYq7t2vEEw5p46HyNJ9FU0t7kPCtbCu5uIkcQeKaX/Kr1F7ZFW9JVlowat9FZ01ficZtDQoOxqpyoPWjKqvjDqaA+mnHlK1XLLmvRf1Y7DGzrfecPln7L6Dsud0D1WE2U472Ct5sofKIM9eCsva4VkzpO8JVNcuiZV5enys/e9Vn1Pa3N9KC/fJwq12zHvySYWiZbNcU+23ga4Rl4a1k7jYzW0KjyCS1pIWM/r3sLQP/RzXMPFpaZBadR4X164oDccNQQQQvnV7sHdfAbIJxqPscrX4dyS9Z/Ji6+B7E3nNfugAE6CYHDE8Ea9tS0YWk2VsZzGjEdEe42OXTJVG9y6tX4zzPGC/jMq02W8NJB1jHv1Vs7YxbwSN3ZuZ83LkEP1KP54W2jtLcGmeA/apBtJ+9vFs5P8A85PoM9F20ZMuPPTjqlf6p8z8uf8Aq39e4HILZjx557ZN6vfTSWFxTqDBh3JNfxkYJws7sai59YEHJ49dVp7lr6f97ZHMZ9RwVHlzJfS7x6tnaZtq+7iYKsP55/0qNldpEg++SKyqcAErNqL5Vi7JTNECeX2S1CiTkeiYptM+8pbDyrK2x1VvZsBAVLQaYx6K7sDgFCE0tbYEaK1pEFVdDVWNJaPGzaMQoPYph/NcVoiqviVWolHvle13qtrPKy5+vUf0TsX1hXAcAMnkvo2yKbtwElfKNiM+cGeK+qbKcd0SVok5Hn/LOaMXQVBearS1KUhVN5aqjWamdcVtpHEpwuAwlmUd05UnVBOqXg36dpMBBnTtKSt7Nj3zGJxOJ7T+EWtdAMgHJgaH7p25qMoW/wDJUHIADVzjoB74FNM2+on6mZ2g1gG40S9S4aBostf/ABQ5xPyFvEcfqoUNstf/AHYRvg1Enmyvw8OMQSlrm1DgRux3/KdsaRIDmRmMk/aF1zvZ3h2gz3VPLKtmpWH2ls2DgCFWUth7ztAB2/AK2Ny0b47+9VcW+ymPG9Eemquz5tZnCXx5vus5sqwZQEtBc8/+0R40TbqLn5d9VoX2LQMYShpNbx+p+yTWrb2pJJORm7zZGN5sgjPQpO3ZDhK1rmnXx/jCqr6xxvNEecKd6MWVgxpamK1ADTX79R1+6qLOsQI5fRPMuSe32SWDwZhgyFaWrkgwhO2+oI0Q/KWr6icAp1h4hV9scJuk9aMM+jjX8165qFvL1r1fFdfB670k45R6zkq45WbE9vSf0a9NHsBjRkwt9sy5BwF8qs6hkBfQfh6pgZkrXyccLye9NZwS1ZqKzTKFXOFRq9VyKq8YP8Kqe48I/StbnKQrNAVayJ2I3nNDjx94VT/+r3FRlO3DCd2XlwHMBgafQu9VZ0a38ZDjrxU/iu3Zc25bMHD2uGSx40MdQSCORKu8Mn66r8vx8mO1HBs8Y0Q2bSLhGhUbzZ1Vj91zZPTIKY2Vsxz3/O3cbzjJ7Ba+s76H8ENeaJc6YkwSMxPDsZT+0t4ZBnupbGaGUwxu7utgafXj+UW5ZxjPX8LD5s/+3Wvx3kZV1f54cIzzwtXY1AWj5uWuO3hZy9s5eHxDumJHY68URlURugjeHgkcuv8ApV8WdXt1VB0IPkn0P6SRE8DPn8pR1y3dyc57qFK6OhMjhOYUsCLJroBBPvqlKz46gqIrgiBw94XgnPWD08qSJ8BtwA/dOh0/Xvkmi3dPT3CC9g5Ryg/Y8lJ1TSffPCWw8vTLHkaKyt63p9lTNfHb8Ju1dPZBLGjtq6fa5Uls6E/Qq8FbiqdRa03r1yVY9emqVfKq4+GPQwxGIRqNKUmMu1/Rt1nTyFvfh8AAQsnaUQCtrsQADA8q3X/y5Vva0FPRBuCih+EvWCzUIrLh2cJQxMngm7iEi5knKXho8LN7MYSlVpB6cvom6lWMAJa5eYwM802bxLOqjamzXuG/SeJ/4n8HyVXto1ARvsIP/U4xhPvqPDpkqTbl/daM+X17VXA1pfuZqDpHcdRxU6u3ddcadf0oOfg/Kq2uJ4c8Ib1NDmcTubx9QyMD6rhTdhxMnn2SjazhomLd5d0VXw47nAifXuutpR6FGQTzHv7KbKUFJTROgM/XwrShTgGef0KRpU1Z0TgDmAD0IhDqWB1Lfdxrp6/5+6Vq0h6p24qDB46Hrw9+FVuqGSDp+dULTZjxrv8AP7Tlu6EoWyU5QYUplg2onaFaVX04IgqTCWlNNcJYvGVIXr38iqw1DEgr1t1Ouqf9E/L5ixidtqaHRYnrdmVoxGzz66ftqAwtNs4ACFSWjFeWQT6npjWrHIFw9TLkF3NZKBWo1JVWp2tzSoGZQpog2iEKuwFNQvAydVIlUtzaxlDZRVzcNBEJIUjJPhN0AgwQUhVpYlWj2YhLPZmOiPQ4pH0co1BkKwdbSosoQULTRKgYge/eUdrZKAxsHyisd78pKbIlPA+vvwUV1Tr086hL1j8o7n00/C83iR6JaeJVHz5+/sIVNvzD6om6psbojIloraOUxQEHoV60KYb76qWF6JuZUyJHVcx2i9qkASl4AdJ8aoFSpBj0XjnQYQahkZUNxmqDU7bqpZcpmhcroZg+XcaS2KtLZ6zdvdhWVveDmms7FUsaFr148pe3rb2im9yx7nKAVw5LtKlWP1UHFKKUqQOEKllFe7ghEoZEqDqfFSYM+9ES4CKFWs4oQpZTJbEKOAjAAFMT4Q2MEo7DkoTsHspRgZpSSlnNx77p4aFRLOKWjC7xLVwZx7IgbjxH5U4+UIHRLV3BTcMeJXOHvwjA6Oz3+1MPj6fRApv0I8qb9CR5CJRd7MKTzI9+qVFTn/tSbWEjqkpuB1CV4BKm9uqEgZ84ZcpyjUJ0QLO0mMStFb7HeQIauxMOdryWkGV3BM0b4jVWrNivA/tSVxswg5RuAnkrQ7Du98YV24LJ7EduOhasaLD5c800Y1+oXeJMpd5TVQYStRU1ZHtEwOp+y57pMBCY/K5jkDG6AxK9q5heNdDYXhfqDwRKFUdkJd51lGJwEvVMygLmO0Qqxz3KnTOvhBdkz1+iiQZ5geF6HfL3KhVOgQw/B6Z9TCFNBXCJXT8p951UHuXMPyn3ogIrDhc0YHlCov1HhSc75ffqjArxphED9UPryQ9/JHD2EQdUqddDK93gRKBOYRKQ4JbDdG/knK8DlAiFFj1OJ158ObIa4CQvoNjslgAwspsj5YWus70AZK7jmQStYMA0WU21ZNEkLUXl6yNVj9tbRbnKkCqKi8B61dtU3mrCf1G8/GkrcbLjcCx/0Rf4r6e1klX0VjVCr64WSxfKWa7XsvKD9VF51UaBxJQMdFRDad6QhOqTlStj9UDDTr6JaoeCO45jylKx1QCPabvk1UD+ETclsLwHMx0UowN7hPYIYdzXVBnuh1SgeJucisdw98UvOEVqkSupox08ITG5UyiQSmgOblMU1GqyHAoh0LcyOqnuxj0UzyK8eEOJ1EnCC5ucIu6vNxTgyrWkpVHmNT6rly7TmKq5qu5n1Kor955n1XLkKhSh/cFv9j/2DsuXLL5/i7xfTVXRV9dcuWRohR3HsgO08LlyU0RfoE3R0HZcuQO9dql/2uXIJEx/b75obvfquXIVIE7VAr6rlyIvDoe35TA0K5chBqdP+4KTtSuXJoSiM0Cnc8PC5cmKjUXOXLlB/wBRpqR1Xq5IZ//Z',1302.50,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(3,'Одиссей',1,1,'https://www.purinaone.ru/dog/sites/purinaonedog.ru/files/2021-04/%D0%A0%D0%BE%D1%82%D0%B2%D0%B5%D0%B8%CC%86%D0%BB%D0%B5%D1%80%20%28506%D1%85379%29_2.jpg',1302.50,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(4,'Эмир',1,1,'https://natworld.info/wp-content/uploads/2018/03/haski.jpeg',1302.50,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(5,'Анда',2,1,'https://www.purina.ru/sites/purina.ru/files/2020-09/prichini_0.jpg',1302.50,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(6,'Терра',2,1,'https://www.purinaone.ru/dog/sites/purinaonedog.ru/files/2020-07/zuby-u-sobak-mobile-min_1.jpg',1302.50,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(7,'Эльба',2,1,'https://www.proplan.ru/sites/owners.proplan.ru/files/styles/nppe_article_listing_image_and_description/public/2020-03/bigl.jpg?itok=gFAdZf28',1302.50,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(8,'Герда',2,1,'https://ru.mypetandi.com/sites/g/files/adhwdz671/files/styles/paragraph_image/public/2020-07/sobaka_v_polyah.jpg?itok=ETeeJJPI',1302.50,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(9,'Эйс',1,1,'https://klike.net/uploads/posts/2019-06/medium/1559799916_2.jpg',1302.50,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(10,'Аксель',1,1,'https://rubryka.com/wp-content/uploads/2021/01/tild3064-3938-4635-a364-306638353835__960.jpg',1302.50,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(11,'Баки',1,1,'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRadIqytXXQVLzeXoaVAIB9Wh4YV59-5VUqdw&usqp=CAU',1302.50,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(12,'Бруно',1,1,'https://cdn.profile.ru/wp-content/uploads/2020/09/img_5f54dadd97ff6.jpeg',1302.50,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(13,'Харли',2,1,'http://exvet.com.ua/share/file/97nksrnga7.jpg',1302.50,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(14,'Лола',2,1,'https://vethelp72.ru/wp-content/uploads/2020/10/%D0%99%D0%BE%D1%80%D0%BA%D1%88%D0%B5%D1%80%D1%81%D0%BA%D0%B8%D0%B9-%D1%82%D0%B5%D1%80%D1%8C%D0%B5%D1%80-%D0%BF%D0%BE%D0%BD%D0%BE%D1%81.jpg',1302.50,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(15,'Рокси',2,1,'https://images11.popmeh.ru/upload/img_cache/965/96554118c6eaea061361b4e754e4259c_ce_1591x848x0x2_cropped_666x444.jpg',1302.50,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(16,'Виксен',2,1,'https://lh3.googleusercontent.com/proxy/XIuNyW0XNsdETxmTLii50feG_wYLNnXc-gGwGXkuKIyzFqW9_ff5kWadj02rycgWlgcVaN8MrW8pXv_77ogzJotFR-U3TLGH1PZoyNceHS_uRGFJ3jgdq095dhh_ujYC_ZChTS7kgX48ixvfveq__Z36R5CcBa7CVrTRM3uEq_5kl6yAXvPyTsuHWRbGkaR-W8269YweYpwuJI9AYb9kcn7G8JxY4ljj8ntM7zchBDju0oSFQTcIZqt402ty3vSVCO28X9F6SW5CnScFsLjrVQKb8g',1302.50,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(17,'Соня',2,2,NULL,345.25,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(18,'Клеопатра',2,2,NULL,345.25,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(19,'Цунами',2,2,NULL,345.25,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(20,'Забияка',2,2,NULL,345.25,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(21,'Матильда',2,2,NULL,345.25,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(22,'Кнопка',2,2,NULL,345.25,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(23,'Масяня',2,2,NULL,345.25,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(24,'Царапка',2,2,NULL,345.25,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(25,'Серсея',2,2,NULL,345.25,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(26,'Ворсинка',2,2,NULL,345.25,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(27,'Гарфилд',1,2,NULL,345.25,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(28,'Том',1,2,NULL,345.25,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(29,'Гудвин',1,2,NULL,345.25,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(30,'Рокки',1,2,NULL,345.25,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(31,'Ленивец',1,2,NULL,345.25,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(32,'Пушок',1,2,NULL,345.25,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(33,'Спорти',1,2,NULL,345.25,1,1,'2021-09-16 08:00:23','2021-09-16 08:00:23'),(34,'Бегемот',1,2,NULL,345.25,1,2,'2021-09-16 08:00:23','2021-09-16 08:00:23');
/*!40000 ALTER TABLE `animals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gender`
--

DROP TABLE IF EXISTS `gender`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gender` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gender`
--

LOCK TABLES `gender` WRITE;
/*!40000 ALTER TABLE `gender` DISABLE KEYS */;
INSERT INTO `gender` VALUES (1,'Male','2021-09-16 06:58:58','2021-09-16 06:58:58'),(2,'Female','2021-09-16 06:58:58','2021-09-16 06:58:58');
/*!40000 ALTER TABLE `gender` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `status` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status`
--

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT INTO `status` VALUES (1,'active','2021-09-16 06:58:53','2021-09-16 06:58:53'),(2,'notactive','2021-09-16 06:58:53','2021-09-16 06:58:53');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'ae77dfe1b145a3bb7060e571cca5d49f0a3d1e62','5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8','zhovklyi.vlad@gmail.com','2021-09-16 08:00:15','2021-09-16 08:00:15');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-16  8:01:37
