<?php

namespace Routers;

class Router
{
    private $routs;
    private $pdo;
    private $render;
    private $asserts;
    protected $predis;

    public function __construct($pdo, $render, $asserts, $predis)
    {
        $this->routs = [];
        $this->pdo = $pdo;
        $this->render = $render;
        $this->asserts = $asserts;
        $this->predis = $predis;
    }

    public function addRoute($route)
    {
        $this->routs[] = $route;
    }

    private function matchRoute($url)
    {
        foreach ($this->routs as $route) {
            if ($route->matchRoute($url)) {
                $route->setArgument($url);

                return $route->getRouteValue();
            }
        }

        return null;
    }

    public function proccess($url)
    {
        $route = $this->matchRoute($url);

        if (!$route) {
            return false;
        }

        return call_user_func(
            [
                new $route['controller']($this->pdo, $this->render, $this->asserts, $this->predis),
                $route['method'],
            ],
            $route['args']
        );
    }
}
