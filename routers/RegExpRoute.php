<?php

namespace Routers;

class RegExpRoute extends Route
{
    public function setArgument($url)
    {
        $this->value['args'] = $this->value['func']($url);
    }

    public function matchRoute($route)
    {
        return (bool) (preg_match($this->route, $route));
    }
}
