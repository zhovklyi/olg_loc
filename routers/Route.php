<?php

namespace Routers;

class Route
{
    protected $route;
    protected $value;

    public function getRouteName()
    {
        return $this->route;
    }

    public function getRouteValue()
    {
        return $this->value;
    }

    public function matchRoute($route)
    {
        return $this->route == $route;
    }

    public function setArgument($url)
    {
        $this->value['args'] = null;
    }

    public function __construct(string $route, array $value)
    {
        $this->route = $route;
        $this->value = $value;
    }
}
